#!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: procesador_de_tiempo.py
# Capitulo: 3 Estilo Publica-Subscribe
# Autor(es): Equipo Los Extranjeros
# Basado en el trabajo de: Perla Velasco & Yonathan Mtz.
# Version: 1.0.0 Abril 2019
# Descripción:
#
#   Esta clase define el rol de un suscriptor, es decir, es un componente que recibe mensajes.
#
#   Las características de ésta clase son las siguientes:
#
#                                   procesador_de_temperatura.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |                         |  - Se suscribe a los   |
#           |                       |                         |    eventos generados   |
#           |                       |  - Procesar valores     |    por el wearable     |
#           |     Procesador de     |    que notifican el     |    Xiaomi My Band.     |
#           |     Tiempo            |    suministro de un     |  - Define el valor ex- |
#           |                       |    medicamento.         |    tremo de la         |
#           |                       |                         |    temperatura.        |
#           |                       |                         |  - Notifica al monitor |
#           |                       |                         |    cuando se detecta   |
#           |                       |                         |    el valor de sumnis- |
#           |                       |                         |    trar el medicamento.|
#           +-----------------------+-------------------------+------------------------+
#
#   A continuación se describen los métodos que se implementaron en ésta clase:
#
#                                               Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |                        |                          |  - Recibe los signos  |
#           |       consume()        |          Ninguno         |    vitales vitales    |
#           |                        |                          |    desde el distribui-|
#           |                        |                          |    dor de mensajes.   |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - ch: propio de Rabbit. |  - Procesa y detecta  |
#           |                        |  - method: propio de     |    valores extremos   |
#           |                        |     Rabbit.              |    de la temperatura. |
#           |       callback()       |  - properties: propio de |                       |
#           |                        |     Rabbit.              |                       |
#           |                        |  - body: mensaje recibi- |                       |
#           |                        |     do.                  |                       |
#           +------------------------+--------------------------+-----------------------+
#           |    string_to_json()    |  - string: texto a con-  |  - Convierte un string|
#           |                        |     vertir en JSON.      |    en un objeto JSON. |
#           +------------------------+--------------------------+-----------------------+
#           |                        |  - medicina: texto con el|  - Regresa la dosis   |
#           |  get_medicina_dosis()  |     medicamento que se   |    del medicamento    |
#           |                        |     suministrará         |    especificado       |
#           +------------------------+--------------------------+-----------------------+
#
#
#           Nota: "propio de Rabbit" implica que se utilizan de manera interna para realizar
#            de manera correcta la recepcion de datos, para éste ejemplo no shubo necesidad
#            de utilizarlos y para evitar la sobrecarga de información se han omitido sus
#            detalles. Para más información acerca del funcionamiento interno de RabbitMQ
#            puedes visitar: https://www.rabbitmq.com/
#
#-------------------------------------------------------------------------
import pika
import sys
sys.path.append('../')
from monitor import Monitor
import time


class ProcesadorTiempo:

    def consume(self):
        try:
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
            # Se solicita un canal por el cuál se enviarán los signos vitales
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue='tiempo', durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(self.callback, queue='tiempo')
            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close()  # Se cierra la conexión
            sys.exit("Conexión finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")

    def callback(self, ch, method, properties, body):
        json_message = self.string_to_json(body)
	medicina_dosis = self.get_medicina_dosis(json_message['medicina'])
        if float(json_message['medicina_si_no']) > 0:
            monitor = Monitor()
            monitor.print_notification(json_message['datetime'], json_message['id'], json_message[
                                       'medicina'], 'Debe suministrar ' + medicina_dosis + ' de', json_message['model'])
        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def string_to_json(self, string):
        message = {}
        string = string.replace('{', '')
        string = string.replace('}', '')
        values = string.split(', ')
        for x in values:
            v = x.split(': ')
            message[v[0].replace('\'', '')] = v[1].replace('\'', '')
        return message

    def get_medicina_dosis(self, medicina):
	medicina_dosis = ''
	if medicina == 'Paracetamol':
	    medicina_dosis = '2 tabletas de 100 mg.'
	else:
	    if medicina == 'Ibuprofeno':
	        medicina_dosis = '600 mg.'
	    else:
	        if medicina == 'Insulina':
                    medicina_dosis = '1 inyección de 10ml.'
                else:
                    if medicina == 'Furosemida':
                        medicina_dosis = '1 tableta de 400mg.'
                    else:
		        if medicina == 'Piroxicam':
                            medicina_dosis = '1 tableta de 20mg.'
	                else:
	                    medicina_dosis = '2 tabletas de 500mg.'
	return medicina_dosis

if __name__ == '__main__':
    p_tiempo = ProcesadorTiempo()
    p_tiempo.consume()
